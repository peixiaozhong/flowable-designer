package com.xnck.demo.flowabledesigner.config;

import cn.hutool.core.util.ArrayUtil;
import com.xnck.demo.flowabledesigner.helper.BeanFactory;
import com.xnck.demo.flowabledesigner.helper.PropertiesConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

/**
 * 配置定义
 */
@Configuration
public class CommonConfig {

    @Bean
    public BeanFactory beanFactory()  {
        return new BeanFactory();
    }

    @Bean
    public PropertiesConfigurer propertiesConfigurer() throws Exception {
        PropertiesConfigurer propertiesConfigurer = new PropertiesConfigurer();
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        propertiesConfigurer.setIgnoreResourceNotFound(true);
        Resource[] classPathResources = resolver.getResources("classpath*:properties*//*.properties");
        Resource applicationResource = resolver.getResource("classpath:application.properties");
        Resource[] resources = ArrayUtil.append(classPathResources, applicationResource);
        propertiesConfigurer.setLocations(resources);
        return propertiesConfigurer;
    }
}
