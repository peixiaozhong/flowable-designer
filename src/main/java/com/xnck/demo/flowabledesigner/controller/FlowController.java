package com.xnck.demo.flowabledesigner.controller;

import org.flowable.engine.*;
import org.flowable.engine.repository.Model;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/flow")
public class FlowController {

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    IdentityService identityService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    @Autowired
    ManagementService managementService;

    /**
     * 启动一个流程
     * @param modelId
     */
    @RequestMapping(value = "start/{modelId}")
    public void start(@PathVariable("modelId") String modelId) {
        Model modelData = repositoryService.getModel(modelId);
        Map<String, Object> map = new HashMap<>();
        map.put("id", "111111");
        ProcessInstance processInstance = null;
        try {
            // 用来设置启动流程的人员ID，引擎会自动把用户ID保存到activiti:initiator中
            identityService.setAuthenticatedUserId("admin");

            processInstance = runtimeService.startProcessInstanceByKey(modelData.getKey(), "myTestFlow1", map);
            String processInstanceId = processInstance.getId();
            System.out.println(processInstanceId);
        } finally {
            identityService.setAuthenticatedUserId(null);
        }
    }

    /**
     * 完成一个任务
     * @param taskId
     */
    @RequestMapping(value = "complate/{taskId}")
    public void complate(@PathVariable("taskId") String taskId) {
        taskService.complete(taskId);
    }
}
