package com.xnck.demo.flowabledesigner.helper;

import org.eclipse.jetty.util.StringUtil;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Bean工厂
 */
public class BeanFactory implements ApplicationContextAware {

    private static ApplicationContext context;

    public BeanFactory() {
    }

    @Override
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        context = ctx;
    }

    public static Object getBean(String beanName) {
        return context != null && !StringUtil.isBlank(beanName) ? context.getBean(beanName) : null;
    }

    public static <T> T getBean(Class<T> className) {
        return context != null && className != null ? context.getBean(className) : null;
    }
}
