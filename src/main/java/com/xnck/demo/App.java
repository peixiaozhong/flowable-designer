package com.xnck.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot 入口
 *
 */
@SpringBootApplication
public class App {

    public static void main( String[] args ) {
        try{
            SpringApplication.run(App.class, args);
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
