# flowable-designer

#### 说明
基于SpringBoot整合了Flowable6.4的流程设计器(不需要idm)，具体过程可参见：[http://www.blackzs.com/archives/1557](http://www.blackzs.com/archives/1557)

#### 启动后的入口地址

http://domain:port/designer/index.html#/editor/

#### 保存后修改流程的地址

http://domain:port/designer/index.html#/editor/{modelId}

#### 启动一个流程

http://domain:port/flow/start/{modelId}

#### 完成一个任务

http://domain:port/flow/complate/{taskId}